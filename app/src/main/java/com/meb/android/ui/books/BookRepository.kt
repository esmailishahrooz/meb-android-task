package com.meb.android.ui.books

import androidx.lifecycle.MutableLiveData
import com.meb.android.api.ApiCall
import com.meb.android.api.CallRetrofit
import com.meb.android.api.models.response.BooksData
import com.meb.android.api.models.response.BooksErr
import org.koin.java.KoinJavaComponent.inject

class BookRepository {
    private val apiCall by inject(ApiCall::class.java)

    fun getBooks(): MutableLiveData<BooksData> {
        val data = MutableLiveData<BooksData>()
        CallRetrofit.callApi(apiCall.getBooks(), {
            data.value = it
        }, {
            data.value = BooksErr(listOf(it.msg))
        })
        return data
    }

    fun bookByOffset(offset: Int): MutableLiveData<BooksData> {
        val data = MutableLiveData<BooksData>()
        CallRetrofit.callApi(apiCall.getBookByOffset( offset), {
            data.value = it
        }, {
            data.value = BooksErr(listOf(it.msg))
        })
        return data
    }

}