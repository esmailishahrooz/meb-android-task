package com.meb.android.ui.bookdialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import com.meb.android.R

class CustomDialogClass(context: Context) : Dialog(context) {

    init {
        setCancelable(false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.fragment_book_details)

    }
}