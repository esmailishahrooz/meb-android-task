package com.meb.android.ui.books

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.meb.android.R
import com.meb.android.adapters.BooksAdapter
import com.meb.android.api.models.response.BooksBean
import com.meb.android.api.models.response.BooksErr
import com.meb.android.databinding.FragmentBooksBinding
import com.meb.android.utils.baseClasses.BaseFragment
import kotlinx.android.synthetic.main.fragment_books.*
import java.util.*


class BookMainFragment : BaseFragment() {
    private lateinit var binding: FragmentBooksBinding
    private val bookViewMode: BookViewModel by viewModels()
    private lateinit var adapter: BooksAdapter

    private var visibleItemCount: Int = 0
    private var totalItemCount: Int = 0
    private var pastVisiblesItems: Int = 0
    private var isLoading: Boolean = false
    private var isLoadMore: Boolean = false

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_books, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initBookList()
        getBooks()

        initSearch()
    }

    private fun initBookList() {
        adapter = BooksAdapter(requireContext()) {
            findNavController().navigate(
                BookMainFragmentDirections.actionBooksFragmentToBookDetails(
                    it
                )
            )
        }
        val layoutManger = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        binding.booksList.layoutManager = layoutManger
        binding.booksList.adapter = adapter
        //setRecyclerViewScrollListener()
        //binding.booksList.addOnScrollListener(scrollListener)
    }

    private lateinit var scrollListener: RecyclerView.OnScrollListener
    private fun setRecyclerViewScrollListener() {
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                visibleItemCount = recyclerView.getLayoutManager()!!.getChildCount()
                totalItemCount = recyclerView.getLayoutManager()!!.getItemCount()
                pastVisiblesItems =
                        (recyclerView.getLayoutManager() as LinearLayoutManager).findFirstVisibleItemPosition()

                if (!isLoading && visibleItemCount + pastVisiblesItems >= totalItemCount && isLoadMore) {
                    isLoading = true
                    getBookByOffset(totalItemCount)
                }
            }
        }
        booksList.addOnScrollListener(scrollListener)
    }

    private fun getBooks() {
        initLoading(binding.loading, true)
        bookViewMode.getBooks()
        bookViewMode.getBooksData().observe(viewLifecycleOwner, Observer {
            when (it) {
                is BooksBean -> {
                    adapter.addItem(it.results)
                    initLoading(binding.loading, false)
                }

                is BooksErr -> {
                    showLongToast("Check Your VPN" + "\n" + "\n" + it.errors.toString())
                    Log.v("retrofitErr", it.errors.toString())
                    initLoading(binding.loading, false)
                }
            }
        })
    }


    private fun getBookByOffset(page: Int) {
        initLoading(binding.loading, true)
        bookViewMode.getBookByOffset(page)
        bookViewMode.getSearchBooksData().observe(viewLifecycleOwner, Observer {
            when (it) {
                is BooksBean -> {
                    adapter.updateItem(it.results)
                    initLoading(binding.loading, false)
                }

                is BooksErr -> {
                    showLongToast("Check Your VPN" + "\n" + "\n" + it.errors.toString())
                    Log.v("retrofitErr", it.errors.toString())
                    initLoading(binding.loading, false)
                }
            }
        })
    }


    private fun initSearch() {
        binding.inputSearch.addTextChangedListener(
            object : TextWatcher {
                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                private var timer: Timer = Timer()
                private val DELAY: Long = 1000 // milliseconds
                override fun afterTextChanged(s: Editable) {
                    timer.cancel()
                    timer = Timer()
                    timer.schedule(
                        object : TimerTask() {
                            override fun run() {
                                requireActivity().runOnUiThread {
                                    if (binding.inputSearch.text.toString().isEmpty())
                                        getBooks()
                                    else
                                        getBookByOffset(
                                            1
                                        )
                                }
                            }
                        },
                        DELAY
                    )
                }
            }
        )
    }

}