package com.meb.android.ui.books

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.meb.android.api.models.response.BooksData
import org.koin.java.KoinJavaComponent.inject

class BookViewModel: ViewModel() {
    private val repository by inject(BookRepository::class.java)

    private var booksData = MutableLiveData<BooksData>()
    fun getBooksData(): MutableLiveData<BooksData> = booksData
    fun getBooks() {
        booksData = repository.getBooks()
    }

    private var searchBooksData = MutableLiveData<BooksData>()
    fun getSearchBooksData(): MutableLiveData<BooksData> = searchBooksData
    fun getBookByOffset(offset: Int) {
        searchBooksData = repository.bookByOffset(offset)
    }
}