package com.meb.android.ui.bookdetails

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import com.meb.android.R
import com.meb.android.api.models.response.BooksResult
import com.meb.android.databinding.FragmentBookDetailsBinding
import com.meb.android.utils.baseClasses.BaseFragment
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.rxkotlin.toObservable
import io.reactivex.schedulers.Schedulers


class BookDetailsFragment : DialogFragment() {

    private lateinit var mDisposable: Disposable
    private var mResult: String = ""

    private lateinit var binding: FragmentBookDetailsBinding
    private val booksResults: BooksResult by lazy {
        BookDetailsFragmentArgs.fromBundle(requireArguments()).data
    }

    companion object {
        fun newInstance(): BookDetailsFragment {
            return BookDetailsFragment()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    private fun getAnyObservable(): Observable<Any> {
        return listOf(true, 1, 2, "Three", 4.0f, 4.5, "Five", false).toObservable()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_book_details, container, false)
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //rxkotlin
        val anyObservable = getAnyObservable()
        mDisposable = anyObservable
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    Log.i("onNext", it.toString())
                    mResult += "$it, "
                },
                onError = {
                    Log.i("onError", it.toString())
                },
                onComplete = {
                    Log.i("onNext", "DONE!!")
                    binding.bookName.text = booksResults.book_details[0].title
                    binding.publishDate.text = booksResults.published_date
                    binding.contributor.text = booksResults.book_details[0].contributor
                    binding.description.text = booksResults.book_details[0].description

                }
            )

/*        binding.bookName.text = booksResults.book_details[0].title
        binding.publishDate.text = booksResults.published_date
        binding.contributor.text = booksResults.book_details[0].contributor
        binding.description.text = booksResults.book_details[0].description*/
    }


}