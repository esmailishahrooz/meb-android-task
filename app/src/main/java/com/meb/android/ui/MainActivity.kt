package com.meb.android.ui

import android.os.Bundle
import com.meb.android.R
import com.meb.android.utils.baseClasses.BaseActivity

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}