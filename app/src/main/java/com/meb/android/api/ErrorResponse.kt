package com.meb.android.api

data class ErrorResponse(val msg: String)