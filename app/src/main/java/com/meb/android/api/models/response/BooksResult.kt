package com.meb.android.api.models.response

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BooksResult(
    val asterisk: Int,
    val bestsellers_date: String,
    val book_details: List<BookDetail>,
    val dagger: Int,
    val isbns: List<Isbn>,
    val list_name: String,
    val rank: Int,
    val rank_last_week: Int,
    val reviews: List<Review>,
    val weeks_on_list: Int,
    val amazon_product_url: String,
    val display_name: String,
    val published_date: String
) : Parcelable

@Parcelize
data class BookDetail(
    val age_group: String,
    val author: String,
    val contributor: String,
    val contributor_note: String,
    val description: String,
    val price: Int,
    val primary_isbn10: String,
    val primary_isbn13: String,
    val publisher: String,
    val title: String
) : Parcelable

@Parcelize
data class Isbn(
    val isbn10: String,
    val isbn13: String
) : Parcelable

@Parcelize
data class Review(
    val article_chapter_link: String,
    val book_review_link: String,
    val first_chapter_link: String,
    val sunday_review_link: String
) : Parcelable