package com.meb.android.api

import com.meb.android.api.models.response.BooksBean
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiCall {

     //GetBook
    @GET("svc/books/v3/lists.json?list-name=hardcover-fiction&api-key=xzjznrMnuymEZPeGeAdK6ttGOz0nuhuJ")
     fun getBooks(): Deferred<BooksBean>


    //Search in Books
    @GET("svc/books/v3/lists.json?list-name=hardcover-fiction&api-key=xzjznrMnuymEZPeGeAdK6ttGOz0nuhuJ")
    fun getBookByOffset(
        @Query("offset") offset: Int
    ): Deferred<BooksBean>
}