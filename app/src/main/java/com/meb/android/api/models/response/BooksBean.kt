package com.meb.android.api.models.response

sealed class BooksData
data class BooksBean(
    val status: String,
    val copyright: String,
    val last_modified: String,
    val results: List<BooksResult>,
    val total_pages: Int,
    val num_results: Int
) : BooksData()

data class BooksErr(val errors: List<String>) : BooksData()