package com.meb.android.di

import com.meb.android.ui.bookdetails.BookDetailsRepository
import com.meb.android.ui.books.BookRepository
import org.koin.dsl.module

object RepositoryModule {
    val repositoryModule = module {
        single { BookRepository() }
        single { BookDetailsRepository() }
    }
}