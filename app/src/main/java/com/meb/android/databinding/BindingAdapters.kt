package com.meb.android.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.meb.android.utils.Constants
import com.squareup.picasso.Picasso

/**
 * Created by ShahroozEsmaili
 */
object BindingAdapters {
    @JvmStatic
    @BindingAdapter("loadImage")
    fun loadImage(v: ImageView?, url: String?) {
        Picasso.get().load("${Constants.IMAGE_BASE_URL}$url").into(v)
    }
}