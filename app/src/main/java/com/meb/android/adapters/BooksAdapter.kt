package com.meb.android.adapters

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.Window
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.meb.android.R
import com.meb.android.api.models.response.BooksResult
import com.meb.android.databinding.BookItemBinding

class BooksAdapter(
    private val context: Context,
    private var onclick: ((BooksResult) -> Unit)?
) :
    RecyclerView.Adapter<BooksAdapter.BooksViewHolder>() {
    init {
        setHasStableIds(true)
    }

    private var BooksResults = mutableListOf<BooksResult>()

    class BooksViewHolder(
        private val binding: BookItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(item: BooksResult, context: Context, onclick: ((BooksResult) -> Unit)?) {

            binding.data = item

            binding.item.setOnClickListener {
                onclick.let { click ->
                    click?.invoke(item)
                }
            }
        }
    }

/*    fun showDialog(title: String) {
        val dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.fragment_book_details)
        val body = dialog.findViewById(R.id.body) as TextView
        body.text = title
        val yesBtn = dialog.findViewById(R.id.yesBtn) as Button
        val noBtn = dialog.findViewById(R.id.noBtn) as TextView
        yesBtn.setOnClickListener {
            dialog.dismiss()
        }
        noBtn.setOnClickListener { dialog.dismiss() }
        dialog.show()
    }*/


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BooksViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = BookItemBinding.inflate(inflater)
        return BooksViewHolder(binding)
    }

    override fun onBindViewHolder(holder: BooksViewHolder, position: Int) {
        holder.bind(BooksResults[position], context, onclick)
    }

    override fun getItemViewType(position: Int): Int = position
    override fun getItemCount(): Int = BooksResults.size
//    override fun getItemId(position: Int): Long = storeData[position].id

    fun addItem(items: List<BooksResult>) {
        val discoverDiffUtilCallback = DiscoverDiffUtilCallback(this.BooksResults, items)
        val diffResult = DiffUtil.calculateDiff(discoverDiffUtilCallback)
        this.BooksResults.addAll(items)
        diffResult.dispatchUpdatesTo(this)
    }

    fun updateItem(items: List<BooksResult>) {
        val discoverDiffUtilCallback = DiscoverDiffUtilCallback(this.BooksResults, items)
        val diffResult = DiffUtil.calculateDiff(discoverDiffUtilCallback)
        this.BooksResults = items as ArrayList<BooksResult>
        diffResult.dispatchUpdatesTo(this)
    }

    class DiscoverDiffUtilCallback(
        private val oldList: List<BooksResult>,
        private val newList: List<BooksResult>
    ) : DiffUtil.Callback() {
        override fun getOldListSize() = oldList.size

        override fun getNewListSize() = newList.size

        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition].amazon_product_url.equals(newList[newItemPosition].amazon_product_url)


        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int) =
            oldList[oldItemPosition] == newList[newItemPosition]
    }
}
