package com.meb.android

import androidx.lifecycle.MutableLiveData
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.meb.android.api.ApiCall
import com.meb.android.api.CallRetrofit
import com.meb.android.api.models.response.BooksBean
import com.meb.android.api.models.response.BooksData
import com.meb.android.api.models.response.BooksErr
import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.java.KoinJavaComponent

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class RetrofitTest {

    private val apiCall by KoinJavaComponent.inject(ApiCall::class.java)

    @Test
    fun testApiResponse() {
        val data = MutableLiveData<BooksData>()
        CallRetrofit.callApi(apiCall.getBooks(), {
            data.value = it
            assertEquals((data.value as BooksBean).status , "OK" )
        }, {
            data.value = BooksErr(listOf(it.msg))
            assertNull(data.value )

        })

    }

}